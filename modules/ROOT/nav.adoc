* Choose and install Fedora (Workstation? Spin? Something else?)
** xref:why-fedora.adoc[Why Fedora Linux?]
** xref:which-image.adoc[Which Fedora edition, spin, or lab to download?]
** xref:create-installationmedia.adoc[How to create an installation media?]
** <further pages related to installation>
* Update, upgrade & package/software management
** <page(s) about "Installing, removing & updating packages/software or upgrading Fedora">
* Customize & configure
** <related pages>
* Important tasks & automation
** xref:BackUp.adoc[BackUp, snapshots and RAID]
* Virtualization: Cockpit, KVM & libvirt
** <related pages>
* Containerization: Cockpit, Toolbox & podman 
** <related pages>
