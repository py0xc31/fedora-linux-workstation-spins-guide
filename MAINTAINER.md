Each page needs regular maintainance to ensure that users can trust the Workstation and Spins User Guide. This includes at least a regular review of all content on a page, updates of the content when necessary or (maybe temporary) removal of content if updates/reviews are not possible for some reason. Maintenance has to be kept in mind when extending pages and when adding new pages! Reviews have to take place for each release! Reviews have to be documented in this file! Maintenance of pages can be shared!  

If maintainers can no longer take care of pages, they have to update the status of the related pages in this file to "orphaned" and if possible, let the Fedora Docs team know (e.g., [open an issue ticket](https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/issues) on GitLab or contact any member on IRC, [Matrix](https://chat.fedoraproject.org/#/room/#docs:fedoraproject.org), or [Discussion](https://discussion.fedoraproject.org/))! The Fedora Docs team will decide how to proceed.  

**NOTE: contributions are not limited to maintainers! Every regular and casual contribution is welcome! Maintainers are only responsible that their pages do not contain obsolete content and that content on these pages keeps aligned. Everything else is open as long as the maintainers' responsibility can be fulfilled.** If a maintainer cannot maintain newly added content (e.g., if it becomes too much), the author of the new content can take over the maintenance for the related section(s) and then become added as second maintainer of the page in this file (shared maintenance: each maintainer responsible for their respective sections). In this case, the alignment of all sections on a page becomes a common responsibility! If conflicts appear in the alignment, the Docs team has to be informed to mediate and if necessary, make a decision (if one maintainer in shared maintenance has to be disadvantaged, the team's decision will focus the policy of the maintainer who maintains most content of the page).  

**TIP: If you have an **idea about a specific page** that should be added, feel free to **open a ticket -> even if you are not able to maintain it yourself!** If we know about your demand, we can possibly identify a maintainer!  

Possible status: drafting, maintained, orphaned, offline, warning  

 - drafting: in development but not yet ready for publishing  
 - maintained: as described above  
 - orphaned: if a page is orphaned but does not yet need updates; another maintainer might take care of "reviewing only" while we search a new maintainer, in order to keep the page online as long as no updates are necessary  
 - offline: (temporary) removals of pages (or some contents of a page) from the published Docs -> this applies if a page/content contains obsolete elements but no one is able to update it at the moment, or if no one is able to review it (e.g., no member has sufficient knowledge about the content). In either case, a separate "tmpOffline" branch should be created to keep the page/content available for working on it, but to keep it offline until it is up to date again.  
 - warning: maybe a future category that can replace "offline" in non-critical cases -> if a page is not up to date but still provides some valuable indication, the page might be displayed with a warning at the top, comparable to what we already have on old release pages with the warning "[Documentation for a newer release is available. View Latest](https://global.discourse-cdn.com/business4/uploads/fedoraproject/original/2X/8/83ef6b3485c27de4250273a18c430740ca7894ee.png)"; e.g., "This page was not reviewed since F34. It cannot be guaranteed that the content is up to date. If you want to change that, let us know at https://". "warning" can be combined with "offline" (the page in general "warning" but some content of it "offline") -> it has to be seen if combinations can be managed from an organizational point of view.  

The structure shall be **problem-oriented** (see the elaboration in the index and in the Web-UI approach). Feel free to suggest changes or additions in a ticket, or in a merge request if you have already something prepared! We can also discuss it in the [Docs Matrix channel](https://chat.fedoraproject.org/#/room/#docs:fedoraproject.org). Every idea and thought is welcome!  

Repo maintainer: py0xc3 | status: **drafting**  

Current page structure of the [nav.adoc](https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/raw/main/modules/ROOT/nav.adoc):  

/ Choose and install Fedora (Workstation? Spin? Something else?)  
// xref:why-fedora.adoc[Why Fedora Linux?] # maintainer: py0xc3 # status: **maintained** # last review: F36  
** xref:which-image.adoc[Which Fedora edition, spin, or lab to download?] # maintainer: py0xc3 # status: **maintained** # last review: F36  
// xref:create-installationmedia.adoc[How to create an installation media?] # maintainer: py0xc3 # status: **maintained** # last review: F36  
// xref:general-installation.adoc[Installing Fedora & post installation] # maintainer: py0xc3 # status: **maintained** # last review: F36  
/ Update, upgrade & package/software management  
/ Customize & configure  
/ Important tasks & automation  
// xref:BackUp.adoc[BackUp, snapshots and RAID] # maintainer: py0xc3 # status: **maintained** # last review: F36  
/ Virtualization: Cockpit, KVM & libvirt  
/ Containerization: Cockpit, Toolbox & podman  

Please use your FAS username!  

The list of pages has to be syncronized with **nav.adoc**!  

If there are any issues, open an [issue ticket](https://gitlab.com/fedora/docs/fedora-linux-documentation/fedora-linux-workstation-spins-guide/-/issues)!  
